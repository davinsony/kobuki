#!/bin/bash

echo "Configurando el KOBUKI"

kobuki_dir=`pwd`/`dirname $0`
cd $kobuki_dir 

echo "Clonando submodulos"
git submodule update --init --recursive

echo "Instalando paquetes de ROS"
sudo apt-get install -y \
ros-melodic-xacro \
ros-melodic-kobuki-msgs \
ros-melodic-kobuki-core \
ros-melodic-yujin-ocs \
ros-melodic-ecl-exceptions \
ros-melodic-ecl-threads \
ros-melodic-ecl-streams \
ros-melodic-rgbd-launch \
ros-melodic-image-view \
cmake build-essential libusb-1.0-0-dev

echo "Instalando freenect"
git clone https://github.com/OpenKinect/libfreenect
cd libfreenect
mkdir build
cd build
cmake -L .. # -L lists all the project options
make
sudo make install
cd ../..
sudo rm -rf libfreenect

echo "Compilando el KOBUKI"
catkin_make
source devel/setup.bash

echo "Configurando el bashrc"
echo "" 											>> ~/.bashrc
echo "#Configuración de Kobuki Turtlebot2" 			>> ~/.bashrc
echo "export TURTLEBOT_BASE=kobuki" 				>> ~/.bashrc
echo "export TURTLEBOT_STACKS=hexagons" 			>> ~/.bashrc
echo "export TURTLEBOT_3D_SENSOR=kinect" 			>> ~/.bashrc
echo "export TURTLEBOT_SIMULATION=false" 			>> ~/.bashrc
echo "#export TURTLEBOT_SERIAL_PORT=/dev/ttyUSB0" 	>> ~/.bashrc